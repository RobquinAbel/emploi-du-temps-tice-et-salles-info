<?php

include_once "../modele/CompteManage.php";
include_once "../modele/SalleManage.php";


$randomImage = "";

$Salle = new Salle();
$AllSalle = $Salle->getSalle();
$lesSalle = "";

foreach ($AllSalle as $InfoSalle)
{
	$Information = $Salle->infoIsSet($InfoSalle["Info"]);
	$lesSalle = $lesSalle."<tr><td><center><a class=\"btn-large grey darken-2\" href=\"#\">".$Salle->completeNum($InfoSalle["N°"])."</a></td><td class=\"grey lighten-1\"></td><td ".$Information."><center>".$InfoSalle["Info"]."</td><td ".$Information."><a href=\"../controleur/rediger.php?N°=".$InfoSalle["N°"]."\"><img class=\"responsive-img\" style=\"width: 20px;\" src=\"../image/rediger.png\" /></a><a href=\"../controleur/effacer.php?N°=".$InfoSalle["N°"]."\"><img class=\"responsive-img\" style=\"width: 20px;\" src=\"../image/eraser.png\" /></a></td></tr> ";
}

if (!isset($_SESSION))
{
	session_start();
}

if (!isset($_SESSION["Identifiant"]) || !isset($_SESSION["MotDePasse"]))
{
	$etat = "Non-Connecté";
	$AddBtn = "";
	echo str_replace("%%BG%%",$randomImage,file_get_contents("../vue/enteteNonCompte.php"));
	echo str_replace(array("%%Info%%","%%Add&Rem%%"),array($lesSalle,$AddBtn),file_get_contents("../vue/vueAccueil.php"));
	include "../vue/pied.php";
}
else
{
	$Identifiant = $_SESSION["Identifiant"];
	$Mdp = $_SESSION["MotDePasse"];
	$test = new Compte($Identifiant, $Mdp);
	if ($test->isLoggedOn())
	{
		$Info = $test->getCompteById($_SESSION["Identifiant"]);
		$AddBtn = 
		"
		<form name=\"page_modif\" action=\"../controleur/ModifSalle.php\" method=\"post\">
		<div class=\"container\">
		<div class=\"z-depth-1 grey lighten-4 row\" style=\"display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE; border-radius: 20px\">
		
		<input class=\"validate\" type=\"number\" name=\"Num\" style=\"width: 30%\"><br>

		<input type=\"submit\" class=\"btn green darken-2\" name=\"action\" value=\"Ajouter\">
		
		<input type=\"submit\"class=\"btn red darken-2\" name=\"action\" value=\"Retirer\">
		
		</div>
		</div>
		</form>
		";

		echo str_replace("%%Balisto%%",$Info["Libelle"],file_get_contents("../vue/enteteCompte.php"));
		echo str_replace(array("%%Info%%","%%Add&Rem%%"),array($lesSalle,$AddBtn),file_get_contents("../vue/vueAccueil.php"));
		include "../vue/pied.php";
	}
	else
	{
		$etat = "Non-Connecté";

		echo str_replace("%%BG%%",$randomImage,file_get_contents("../vue/enteteNonCompte.php"));
		echo str_replace("%%Info%%",$lesSalle,file_get_contents("../vue/vueAccueil.php"));
		include "../vue/pied.php";
	}
}

?>
