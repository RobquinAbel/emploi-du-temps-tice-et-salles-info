<?php
//mettre en include_once ce qui correspond dans le modele
    
include_once "../modele/CompteManage.php";
include_once "../modele/SalleManage.php";

if (!isset($_SESSION))
{
    session_start();
}

$Salle = new Salle();

if ($_POST["action"]=="Ajouter")
{
    $Salle->AddSalle($_POST["Num"]);
}
elseif ($_POST["action"]=="Retirer")
{
    $Salle->DelSalle($_POST["Num"]);
}
else
{
    echo "Qu'est ce que tu fous ici ?";
}

if (!isset($_SESSION["Identifiant"]) || !isset($_SESSION["MotDePasse"]))
{

    include "../vue/enteteNonCompte.php";
    include "../vue/vueThx.php";
    include "../vue/pied.php";
}
else
{
$Identifiant = $_SESSION["Identifiant"];
$Mdp = $_SESSION["MotDePasse"];
$test = new Compte($Identifiant, $Mdp);
if ($test->isLoggedOn())
{
    $Info = $test->getCompteById($_SESSION["Identifiant"]);

    echo str_replace("%%Balisto%%",$Info["Libelle"],file_get_contents("../vue/enteteCompte.php"));
    include "../vue/vueThx.php";
    include "../vue/pied.php";
}
else
{

    include "../vue/enteteNonCompte.php";
    include "../vue/vueThx.php";
    include "../vue/pied.php";
}
}


?>