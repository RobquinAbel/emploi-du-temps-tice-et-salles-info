<?php
//mettre en include_once ce qui correspond dans le modele
    


include_once "../modele/CompteManage.php";
include_once "../modele/SalleManage.php";

$Salle = new Salle();
$erase = null;
$Salle->setInfo($erase,$_GET["N°"]);

if (!isset($_SESSION["Identifiant"]) || !isset($_SESSION["MotDePasse"]))
{

    include "../vue/enteteNonCompte.php";
    include "../vue/vueThx.php";
    include "../vue/pied.php";
}
else
{
$Identifiant = $_SESSION["Identifiant"];
$Mdp = $_SESSION["MotDePasse"];
$test = new Compte($Identifiant, $Mdp);
if ($test->isLoggedOn())
{
    $Info = $test->getCompteById($_SESSION["Identifiant"]);

    echo str_replace("%%Balisto%%",$Info["Libelle"],file_get_contents("../vue/enteteCompte.php"));
    include "../vue/vueThx.php";
    include "../vue/pied.php";
}
else
{

    include "../vue/enteteNonCompte.php";
    include "../vue/vueThx.php";
    include "../vue/pied.php";
}
}


?>