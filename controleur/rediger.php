<?php
//mettre en include_once ce qui correspond dans le modele
    


include_once "../modele/CompteManage.php";
include_once "../modele/SalleManage.php";

$Salle = new Salle();
$AllSalle = $Salle->getSalle();
$lesSalle = "";

if (!isset($_SESSION))
        {
          session_start();
        }

if (!isset($_SESSION["Identifiant"]) || !isset($_SESSION["MotDePasse"]))
{
    include "../vue/enteteNonCompte.php";
    echo str_replace("%%Num%%",$_GET["N°"],file_get_contents("../vue/vueRediger.php"));
    include "../vue/pied.php";
    $_SESSION["Num"]=$_GET["N°"];
}
else
{

    $ID = $_SESSION["Identifiant"];
    $Mdp = $_SESSION["MotDePasse"];
    $test = new Compte($ID, $Mdp);

    if($test->isLoggedOn())
    {

    $Info = $test->getCompteById($_SESSION["Identifiant"]);

    echo str_replace("%%Balisto%%",$Info["Libelle"],file_get_contents("../vue/enteteCompte.php"));
    echo str_replace("%%Num%%",$_GET["N°"],file_get_contents("../vue/vueRediger.php"));
    include "../vue/pied.php";
    $_SESSION["Num"]=$_GET["N°"];
    }
    else
    {

        include "../vue/enteteNonCompte.php";
        echo str_replace("%%Num%%",$_GET["N°"],file_get_contents("../vue/vueRediger.php"));
        include "../vue/pied.php";
        $_SESSION["Num"]=$_GET["N°"];
    }

    

}
?>