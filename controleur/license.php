<?php

include("../modele/CompteManage.php");

if (!isset($_SESSION))
        {
          session_start();
        }


if (!isset($_SESSION["Identifiant"]) || !isset($_SESSION["MotDePasse"]))
{
    $etat = "Non-Connecté";

    include "../vue/enteteNonCompte.php";
	echo str_replace("%%back%%",$_SERVER['HTTP_REFERER'],file_get_contents("../vue/vueLicense.php"));
    include "../vue/pied.php";
}
else
{
$Identifiant = $_SESSION["Identifiant"];
$Mdp = $_SESSION["MotDePasse"];
$test = new Compte($Identifiant, $Mdp);
if ($test->isLoggedOn())
{
    $Info = $test->getCompteById($_SESSION["Identifiant"]);

    echo str_replace("%%Balisto%%",$Info["Libelle"],file_get_contents("../vue/enteteCompte.php"));
    echo str_replace("%%back%%",$_SERVER['HTTP_REFERER'],file_get_contents("../vue/vueLicense.php"));
    include "../vue/pied.php";
}
else
{
    $etat = "Non-Connecté";

    include "../vue/enteteNonCompte.php";
    echo str_replace("%%back%%",$_SERVER['HTTP_REFERER'],file_get_contents("../vue/vueLicense.php"));
    include "../vue/pied.php";
}
}

?>