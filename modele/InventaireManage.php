<?php

include_once "../modele/BDManage.php";

class Inv
{
  private $BDD;

  /**
   * Constructeur de la classe
   */
  function __construct()
  {
      // Création de l'objet communiquant avec la bdd
    $this->BDD = new Data;

      // Ouverture d'une session si elle ne l'est pas encore
    if (!isset($_SESSION))
    {
      session_start();
    }
  }

  /**
   * Obtenir l'inventaire des appareils d'un type
   *
   * @param      <type>  $type   Le type
   *
   * @return     array   L'inventaire
   */
  function getInvByType($type)
  {
    $resultat = array();
    try
    {
        // Connexion à la bdd
      $cnx = $this->BDD->connexionPDO();
      // Préparation de la requête
      $req = $cnx->prepare("select * from inventaire where type=:type");
      $req->bindValue(':type', $type, PDO::PARAM_STR);
        // Execution de la requête
      $req->execute();
        // Réupération de la réponse SQL
      $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
      // En cas d'erreur
    catch (PDOException $e)
    {
      print "Erreur !: " . $e->getMessage();
      die();
    }
    return $resultat;
  }

  /**
   * Obtenir la liste des types
   *
   * @return     array  Les types
   */
  function getType()
  {
    $resultat = array();

      // Préparation et envoie d'une requête SQL
    try
    {
        // Connexion à la bdd
      $cnx = $this->BDD->connexionPDO();
        // Préparation de la requête
      $req = $cnx->prepare("select distinct(Type) from inventaire order by 1 desc");
      $req->execute();
        // Réupération de la réponse SQL
      $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
      // En cas d'erreur
    catch (PDOException $e)
    {
      print "Erreur !: " . $e->getMessage();
      die();
    }
    return $resultat;
  }

  /**
   * Mettre un appareil dans l'inventaire
   *
   * @param      string  $Qtte   La quantité
   * @param      string  $Nom    Le nom
   * @param      string  $Loca   La localisation
   * @param      string  $Type   Le type
   */
  function setInv($Qtte,$Nom,$Loca,$Type)
  {
      // Préparation et envoie d'une requête SQL
    try
    {
        // Connexion à la bdd
      $cnx = $this->BDD->connexionPDO();
        // Préparation de la requête
      $req = $cnx->prepare("insert into `inventaire` (`Nom`, `Type`, `Quantité`, `Localisation`) VALUES ('".$Nom."', '".$Type."', '".$Qtte."', '".$Loca."')");
        // Execution de la requête
      $req->execute();
    }
      // En cas d'erreur
    catch (PDOException $e)
    {
      //Something went wrong so I need to redirect
      header("Location: ../index.php");
      // Always make an explicit call to exit() after a redirection header.
      exit();
    }
  }

  /**
   * Enlever un appareil de l'inventaire
   *
   * @param      string  $Nom    Le nom
   * @param      string  $Loca   La localisation
   */
  function delInv($Nom,$Loca)
  {
      // Préparation et envoie d'une requête SQL
    try
    {
        // Connexion à la bdd
      $cnx = $this->BDD->connexionPDO();
        // Préparation de la requête
      $req = $cnx->prepare("delete from `inventaire` where Nom='".$Nom."' and Localisation='".$Loca."'");
        // Execution de la requête
      $req->execute();
    }
      // En cas d'erreur
    catch (PDOException $e)
    {
      //Something went wrong so I need to redirect
      header("Location: ../index.php");
      // Always make an explicit call to exit() after a redirection header.
      exit();
    }
  }
}

?>