<?php

include_once "../modele/BDManage.php";

class Compte
{
  private $BDD;

  /**
   * Constructeur de la classe
   *
   * @param      <type>  $Identifiant  L'identifiant
   * @param      <type>  $Mdp          Le mot de passe
   */
  function __construct( $Identifiant, $Mdp)
  {
      // Création de l'objet communiquant avec la bdd
    $this->BDD = new Data;

      // Ouverture d'une session si elle ne l'est pas encore
    if (!isset($_SESSION))
    {
      session_start();
    }

      // Fin de session si identifiant invalide
    if (!$this->checkId($Identifiant))
    {
      session_destroy();
      return;
    }
    else
    {
        // Récupération des informations du compte correspondant à l'identifiant transmis
      $util = $this->getCompteById($Identifiant);
        // Récupération du mot de passe du compte correspondant à l'identifiant transmis
      $mdpBD = $util["MotDePasse"];

        // Vérification que le mot de passe transmis correspond à celui de la bdd
      if (trim($mdpBD) == trim($Mdp))
      {
        if (!isset($_SESSION))
        {
          session_start();
        }
          // Le mot de passe et l'identifiant son stockés dans la session
        $_SESSION["Identifiant"] = $Identifiant;
        $_SESSION["MotDePasse"] = $mdpBD;
      }
      else
      {
        return false;
      }
    }
  }

    /**
     * Récupérer les informations du compte correspondant à l'identifiant transmis
     *
     * @param      <type>  $Identifiant  L'identifiant
     *
     * @return     array   Les informations
     */
    function getCompteById( $Identifiant)
    {
      $resultat = array();

      // Préparation et envoie d'une requête SQL
      try
      {
          // Connexion à la bdd
        $cnx = $this->BDD->connexionPDO();
          // Préparation de la requête
        $req = $cnx->prepare("select * from compte where Identifiant=:Identifiant");
        $req->bindValue(':Identifiant', $Identifiant, PDO::PARAM_STR);
          // Execution de la requête
        $req->execute();
          // Réupération de la réponse SQL
        $resultat = $req->fetch(PDO::FETCH_ASSOC);

      }
        // En cas d'erreur
      catch (PDOException $e)
      {
        print "Erreur !: " . $e->getMessage();
        die();
      }
      return $resultat;
    }

    /**
     * Vérification de l'état de connexion
     *
     * @return     bool  True pour connecté / False sinon
     */
    public function isLoggedOn()
    {
        // Ouverture d'une session si elle ne l'est pas encore
      if (!isset($_SESSION))
      {
        session_start();
      }

      $ret = false;

        // Regarde si il y a un identifiant et un mot de passe dans la session
      if(isset($_SESSION["Identifiant"]) && isset($_SESSION["MotDePasse"]))
      {
          // Récupération des informations du compte correspondant à l'identifiant transmis
        $util = $this->getCompteById($_SESSION["Identifiant"]);
          // Vérifie la cohérence de l'identifiant et du mot de passe dans la session
        if ($util["Identifiant"] == $_SESSION["Identifiant"] && $util["MotDePasse"] == $_SESSION["MotDePasse"])
        {
          $ret = true;
        }
      }
      return $ret;
    }

    /**
     * Déconnexion
     */
    public function logout()
    {
      unset($_SESSION["Identifiant"]);
      unset($_SESSION["MotDePasse"]);
      session_destroy();
    }

    /**
     * { function_description }
     *
     * @param      <type>  $Identifiant  L'identifiant
     */
    public function checkId($Identifiant)
    {
      $resultat = array();

        // Préparation et envoie d'une requête SQL
      try
      {
          // Connexion à la bdd
        $cnx = $this->BDD->connexionPDO();
          // Préparation de la requête
        $req = $cnx->prepare("select Identifiant from compte where Identifiant=:Identifiant");
        $req->bindValue(':Identifiant', $Identifiant, PDO::PARAM_STR);
          // Execution de la requête
        $req->execute();
          // Réupération de la réponse SQL
        $resultat = $req->fetch(PDO::FETCH_ASSOC);
      }
        // En cas d'erreur
      catch (PDOException $e)
      {
        print "Erreur !: " . $e->getMessage();
        die();
      }
      return $resultat;
    }
  }
  ?>