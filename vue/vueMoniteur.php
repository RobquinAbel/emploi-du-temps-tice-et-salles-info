<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" http-equiv="refresh" content="1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <style>
        body 
        {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main
        {
            flex: 1 0 auto;
        }

        body
        {
            background: #e4e4e4;
        }
        
        nav.nav-center ul
        {
            text-align: center;
        }
        nav.nav-center ul li
        {
            display: inline;
            float: none;
        }
        nav.nav-center ul li a
        {
            display: inline-block;
        }
    </style>
</head>

<body style=" background: url(%%BG%%)">
    <nav>
      <div class="grey darken-3 nav-wrapper">
        <ul id="nav-mobile" class="left hide-on-med-and-down grey darken-2">
          <li><a href="../controleur/accueil.php">Info Salle</a></li>
      </ul>
  </div>
</nav>


<div class="section"></div>
<main>
    <center>
        <div class="container">
            <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE; border-radius: 20px; width: 80%">               
            <table class="striped grey lighten-2 bordered" style="overflow: scroll;">

                <tablehead>
                    <tr>
                        <td class="grey lighten-1" style="text-align: center; width: 30%">N° de la salle</td>
                        <td class="grey lighten-1" style="width: 1%"></td>
                        <td class="grey lighten-1" style="text-align: center">Information relative à la salle</td>
                        
                    </tr>
                </tablehead>
                <tablebody>
                    %%Info%%
                </tablebody>
            </table>
        </a>
        <br>
    </div>

</div>

</center>

</main>
<div class="section"></div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>


<footer>
    <nav class="grey darken-3 nav-center" role="navigation">
        <div class=" nav-wrapper container">
            <ul>
                <li><a href="../controleur/license.php">License</a></li>
                <li><a href="#">Crédit</a></li>
            </ul>
        </div>
    </nav>
</footer>
</body>
</html>

