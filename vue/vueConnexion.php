        <form class="col s12" name="page_suivante" action="../controleur/connexion2.php" method="post" autocomplete="off"> 
            <div class="section"></div>
            <main>
                <center>
                    <div class="container">
                        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE; border-radius: 20px">

                            %%error%%
                            <div class='row'>
                                <div class='input-field col s12'>
                                    <input class='validate' type='text' name='Identifiant' id='Identifiant' />
                                    <label for='email'>Identifiant</label>
                                </div>
                            </div>

                            <div class='row'>
                                <div class='input-field col s12'>
                                    <input class='validate' type='password' name='MotDePasse' id='MotDePasse' />
                                    <label for='password'>Mot de passe</label>
                                </div>
                            </div>

                            <br />
                            <center>

                                <div class='row'>
                                    
                                    <input type='submit' name='btn_login' class='col s12 btn btn-large waves-effect grey' href="../controleur/connexion2.php" style='border-radius: 20px;' value="connexion">
                                    
                                </div>
                            </center>
                            
                            
                        </div>
                    </div>
                    <a class='cyan-text' href="../controleur/accueil.php">Retour à l'Accueil</a>
                </center>
                <div class="section"></div>
            </main>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>

            
        </form>
