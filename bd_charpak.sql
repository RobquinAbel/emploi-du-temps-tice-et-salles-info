-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 02 fév. 2021 à 11:21
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bd_charpak`
--

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

DROP TABLE IF EXISTS `compte`;
CREATE TABLE IF NOT EXISTS `compte` (
  `Libelle` varchar(30) NOT NULL,
  `Identifiant` varchar(30) NOT NULL,
  `MotDePasse` varchar(30) NOT NULL,
  PRIMARY KEY (`Libelle`,`Identifiant`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`Libelle`, `Identifiant`, `MotDePasse`) VALUES
('Administration', 'admin', 'aze'),
('TICE', 'tice', 'aze');

-- --------------------------------------------------------

--
-- Structure de la table `inventaire`
--

DROP TABLE IF EXISTS `inventaire`;
CREATE TABLE IF NOT EXISTS `inventaire` (
  `Nom` varchar(30) NOT NULL,
  `Type` varchar(30) NOT NULL,
  `Quantité` int(11) NOT NULL,
  `Localisation` varchar(30) NOT NULL,
  PRIMARY KEY (`Nom`,`Localisation`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `inventaire`
--

INSERT INTO `inventaire` (`Nom`, `Type`, `Quantité`, `Localisation`) VALUES
('Predator Helios 300', 'Ordinateur', 7, 'Salle Informatique'),
('Acer Sisilafamille', 'Ordinateur', 12, 'Salle Techn02'),
('Vidéo Projecteur', 'Autre', 3, 'Salle Informatique'),
('Valise de classe mobile', 'Classe Mobile', 2, 'Gymnase');

-- --------------------------------------------------------

--
-- Structure de la table `réservation`
--

DROP TABLE IF EXISTS `réservation`;
CREATE TABLE IF NOT EXISTS `réservation` (
  `IDReserv` int(11) NOT NULL AUTO_INCREMENT,
  `N°Salle` int(3) NOT NULL,
  `Réserveur` varchar(30) NOT NULL,
  `Motif` varchar(200) NOT NULL,
  `Date` date NOT NULL,
  `HeureDébut` time NOT NULL,
  `HeureFin` time NOT NULL,
  `Code` varchar(30) NOT NULL,
  PRIMARY KEY (`IDReserv`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `réservation`
--

INSERT INTO `réservation` (`IDReserv`, `N°Salle`, `Réserveur`, `Motif`, `Date`, `HeureDébut`, `HeureFin`, `Code`) VALUES
(1, 2, 'Tice', 'Réparation du Vidéo-Projecteur', '2021-01-08', '09:25:00', '11:35:00', 'azerty');

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `N°` int(3) NOT NULL,
  `Info` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`N°`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`N°`, `Info`) VALUES
(107, NULL),
(106, NULL),
(105, NULL),
(104, NULL),
(103, NULL),
(102, NULL),
(101, NULL),
(8, NULL),
(7, NULL),
(6, NULL),
(5, NULL),
(4, NULL),
(108, NULL),
(114, NULL),
(115, NULL),
(142, NULL),
(143, NULL),
(144, 'Projecteur à dépoussiérer'),
(145, NULL),
(146, NULL),
(44, NULL),
(45, NULL),
(47, NULL),
(48, NULL),
(50, NULL),
(51, NULL),
(52, NULL),
(53, NULL),
(777, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
